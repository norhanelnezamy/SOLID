<?php
namespace SOLID\LSP;

class Encrypt {

    protected $string;

    public function setString($string)
    {
        $this->string = $string;
    }

    public function getString()
    {
        return $this->string;
    }


    public function encryptString()
    {
        return md5($this->getString());
    }

}
?>