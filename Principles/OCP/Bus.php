<?php
namespace SOLID\OCP;

class Bus extends Vehicle
{
  private $bus_numder;
  private $passenger_count;
  private $doors;

  public function __construct($bus_numder)
  {
    $this->setBusNumber($bus_numder);
  }

  public function setBusNumber($bus_numder)
  {
    $this->bus_numder = $bus_numder;
  }

  public function getBusNumber()
  {
    return $this->bus_numder;
  }

  public function setPassengerCount($passenger_count)
  {
    $this->passenger_count = $passenger_count;
  }

  public function getPassengerCount()
  {
    return $this->passenger_count;
  }

  public function setDoors($doors)
  {
    $this->doors = $doors;
  }

  public function getDoors()
  {
    return $this->doors;
  }

  public function move() : string
  {
    return "I'm moving by a bus driver.";
  }
}

?>
