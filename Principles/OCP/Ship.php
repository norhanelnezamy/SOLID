<?php
namespace SOLID\OCP;

class Ship extends Vehicle
{

  public function move() : string
  {
    return "I'm moving by a shipmaster.";
  }
}

?>
