<?php
namespace SOLID\OCP;

class Plane extends Vehicle
{

  public function move() : string
  {
    return "I'm Flying by a pilot.";
  }
  
}

?>
