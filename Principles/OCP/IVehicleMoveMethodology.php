<?php
namespace SOLID\OCP;

interface IVehicleMoveMethodology
{
  public function methodology() : string;
}

?>
