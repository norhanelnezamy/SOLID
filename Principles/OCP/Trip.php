<?php
namespace SOLID\OCP;

class Trip
{
  private $vehicle ;
  private $trip_num ;
  private $duration ;
  private $price ;

  function __construct(Vehicle $vehicle, string $trip_num , int $duration , float $price)
  {
    $this->setVehicle($vehicle);
    $this->setTripNum($trip_num);
    $this->setDuration($duration);
    $this->setPrice($price);
  }


  public function setTripNum($trip_num)
  {
    $this->trip_num = $trip_num;
  }

  public function getTripNum()
  {
    return $this->trip_num;
  }

  public function setDuration($duration)
  {
    $this->duration = $duration;
  }

  public function getDuration()
  {
    return $this->duration;
  }

  public function setPrice($price)
  {
    $this->price = $price;
  }

  public function getPrice()
  {
    return $this->price;
  }

  public function getVehicle(Vehicle $vehicle)
  {
    return $this->vehicle;
  }

  public function setVehicle(Vehicle $vehicle)
  {
    $this->vehicle = $vehicle;
  }

  public function move()
  {
    return $this->vehicle->move();
  }

  public function methodology()
  {
    return $this->vehicle->getMoveMethodology();
  }



}


 ?>
