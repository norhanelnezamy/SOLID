<?php
namespace SOLID\OCP\MoveMethodology;

use SOLID\OCP\IVehicleMoveMethodology;

class SailingInSea implements IVehicleMoveMethodology
{

  public function methodology() : string
  {
    return "I'm sailing in sea.";
  }

}

?>
