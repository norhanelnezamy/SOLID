<?php
namespace SOLID\OCP\MoveMethodology;

use SOLID\OCP\IVehicleMoveMethodology;

class MoveOnRoad implements IVehicleMoveMethodology
{

  public function methodology() : string
  {
    return "I'm moving on road.";
  }
}

?>
