<?php
namespace SOLID\OCP\MoveMethodology;

use SOLID\OCP\IVehicleMoveMethodology;

class FlyInSky implements IVehicleMoveMethodology
{

  public function methodology() : string
  {
    return "I'm flying in sky.";
  }
}

?>
