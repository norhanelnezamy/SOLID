<?php
namespace SOLID\OCP;

class Route
{
  private $source;
  private $destination;
  private $distance;

  function __construct($source, $destination, $distance)
  {
    $this->setSource($source);
    $this->setDestination($destination);
    $this->setDistance($distance);
  }

  public function setSource($source)
  {
    $this->source = $source;
  }

  public function getSource()
  {
    return $this->source;
  }

  public function setDestination($destination)
  {
    $this->destination = $destination;
  }

  public function getDestination()
  {
    return $this->destination;
  }

  public function setDistance($distance)
  {
    $this->distance = $distance;
  }

  public function getDistance()
  {
    return $this->distance;
  }
}

?>
