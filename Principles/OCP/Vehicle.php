<?php
namespace SOLID\OCP;

abstract class Vehicle implements IVehicle
{

  private $driver;
  private $max_speed;
  private $color;
  private $routes=[];
  private $move_methodology;

  public function setDriver($driver)
  {
    $this->driver = $driver;
  }

  public function getDriver()
  {
    return $this->driver;
  }

  public function setMaxSpeed($max_speed)
  {
    $this->max_speed = $max_speed;
  }

  public function getMaxSpeed()
  {
    return $this->max_speed;
  }

  public function setColor($color)
  {
    $this->color = $color;
  }

  public function getColor()
  {
    return $this->color;
  }

  public function setRoutes($routes)
  {
    $this->routes = $routes;
  }

  public function addRoute(Route $route)
  {
    $this->routes[] = $route;
  }

  public function getRoutes()
  {
    return $this->routes;
  }

  public function setMoveMethodology(IVehicleMoveMethodology $move_methodology)
  {
    $this->move_methodology = $move_methodology;
  }

  public function getMoveMethodology()
  {
    return $this->move_methodology->methodology();
  }

}


 ?>
