<?php
namespace SOLID\OCP;

class Car extends Vehicle
{

  public function move() : string
  {
    return "I'm moving by a car driver.";
  }
}

?>
