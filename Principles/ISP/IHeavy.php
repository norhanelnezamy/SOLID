<?php
namespace SOLID\ISP;

interface IHeavy 
{
  public function carryWeight() : string;
}

?>
