<?php
namespace SOLID\ISP;

class Plane implements IVehicle, IHeavy
{

  public function move() : string
  {
    return "I'm Flying by a pilot.";
  }

  public function stop() : string
  {
    return "Stoping......";
  }

  public function carryWeight(): string
  {
    return "Can carrying weight till 1000 Kg";
  }
  
}

?>
