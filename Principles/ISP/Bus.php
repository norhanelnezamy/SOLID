<?php
namespace SOLID\ISP;

class Bus implements IVehicle
{
  public function move() : string
  {
    return "I'm moving by a bus driver.";
  }

  public function stop() : string
  {
    return "Stoping......";
  }
}

?>
