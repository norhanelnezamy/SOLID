<?php
namespace SOLID\ISP;

interface IEnterment 
{
  public function palyMusic() : string;

  public function pause() : string;

  public function stopMusic() : string;
}

?>
