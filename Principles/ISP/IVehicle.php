<?php
namespace SOLID\ISP;

interface IVehicle 
{
  public function move() : string;

  public function stop() : string;
}

?>
