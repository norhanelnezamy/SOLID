<?php
namespace SOLID\ISP;

class Car implements IVehicle, IEnterment
{

  public function move() : string
  {
    return "I'm moving by a car driver.";
  }

  public function stop() : string
  {
    return "Stoping......";
  }

  public function palyMusic(): string
  {
    return "play music";
  }

  public function stopMusic(): string
  {
    return "stop music";
  }

  public function pause(): string
  {
    return "pause current track";
  }
}

?>
