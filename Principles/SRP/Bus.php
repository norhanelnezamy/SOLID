<?php
namespace SOLID\SRP;

class Bus
{

  private $bus_numder;
  private $driver;
  private $max_speed;
  private $passenger_count;
  private $doors;
  private $color;
  private $routes=[];

  public function __construct($bus_numder)
  {
    $this->setBusNumber($bus_numder);
  }

  public function setBusNumber($bus_numder)
  {
    $this->bus_numder = $bus_numder;
  }

  public function getBusNumber()
  {
    return $this->bus_numder;
  }

  public function setDriver($driver)
  {
    $this->driver = $driver;
  }

  public function getDriver()
  {
    return $this->driver;
  }

  public function setMaxSpeed($max_speed)
  {
    $this->max_speed = $max_speed;
  }

  public function getMaxSpeed()
  {
    return $this->max_speed;
  }

  public function setDoors($doors)
  {
    $this->doors = $doors;
  }

  public function getDoors()
  {
    return $this->doors;
  }

  public function setColor($color)
  {
    $this->color = $color;
  }

  public function getColor()
  {
    return $this->color;
  }

  public function setRoutes($routes)
  {
    $this->routes = $routes;
  }

  public function addRoute(Route $route)
  {
    $this->routes[] = $route;
  }

  public function getRoutes()
  {
    return $this->routes;
  }
}

?>
