<?php
namespace SOLID\SRP;

class Driver
{
  private $name;
  private $age;
  private $phone;
  private $address;

  function __construct($name, $age, $phone, $address)
  {
    $this->setName($name);
    $this->setAge($age);
    $this->setPhone($phone);
    $this->setAddress($address);
  }

  public function setName($name)
  {
    $this->name = $name;
  }

  public function getName()
  {
    return $this->name;
  }

  public function setAge($age)
  {
    $this->age = $age;
  }

  public function getAge()
  {
    return $this->age;
  }

  public function setPhone($phone)
  {
    $this->phone = $phone;
  }

  public function getPhone()
  {
    return $this->phone;
  }

  public function setAddress($address)
  {
    $this->address = $address;
  }

  public function getAddress()
  {
    return $this->address;
  }
}

 ?>
