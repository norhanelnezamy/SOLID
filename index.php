<?php
use \SOLID\OCP\Bus;
use \SOLID\OCP\Car;
use \SOLID\OCP\Ship;
use \SOLID\OCP\Plane;
use \SOLID\OCP\Driver;
use \SOLID\OCP\Route;
use \SOLID\OCP\Trip;
use \SOLID\OCP\MoveMethodology\FlyInSky;
use \SOLID\OCP\MoveMethodology\MoveOnRoad;
use \SOLID\OCP\MoveMethodology\SailingInSea;
use \SOLID\LSP\Rectangle;
use \SOLID\LSP\Square;
use \SOLID\LSP\Encrypt;
use \SOLID\LSP\EncryptSubType;

require_once __DIR__ . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

//LSP Testing
// every object will calculate its area without any change in getArea method defination
$shape = new Rectangle();
// $shape = new Square();
$shape->setWidth(5);
$shape->setHeight(12);

echo $shape->getArea();

// every object will encrypt its string without any change in encryptString method defination
// $encrypt = new Encrypt();
$encrypt = new EncryptSubType();
$encrypt->setString('test');
echo $encrypt->encryptString();

// OCP Testing
$driver = new Driver('Mohammed Ali', 30, '02345445665', 'Mansoura');

$route_1 = new Route('caire', 'mansoura', 126.8);
$route_2 = new Route('caire', 'mahalla', 126.5);

$bus = new Bus(3453454);
$bus->setDriver($driver);
$bus->setMaxSpeed(300);
$bus->setColor('white');
$bus->addRoute($route_1);

$bus->setMoveMethodology(new MoveOnRoad());

$trip_1 = new Trip($bus, '2re324', 30, 30.00);
// echo $trip_1->move();
// echo $trip_1->methodology();


$ship = new Ship(3453454);
$ship->setDriver($driver);
$ship->setMaxSpeed(300);
$ship->setColor('grey');
$ship->addRoute($route_2);

$ship->setMoveMethodology(new SailingInSea());

$trip_2 = new Trip($ship, 'sefre465', 20, 50.00);
// echo $trip_2->move();
// echo $trip_2->methodology();


?>
